--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7
-- Dumped by pg_dump version 11.7

-- Started on 2020-05-26 07:45:28

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 16597)
-- Name: x_biodata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_biodata (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on time with time zone NOT NULL,
    modified_by bigint,
    modified_on time with time zone,
    delete_by bigint,
    delete_on time with time zone,
    is_delete boolean NOT NULL,
    fullname character varying(255) NOT NULL,
    nick_name character varying(100) NOT NULL,
    pob character varying(100) NOT NULL,
    dob date NOT NULL,
    gender boolean NOT NULL,
    religion_id bigint NOT NULL,
    high integer,
    weight integer,
    nationality character varying(100),
    ethnic character varying(50),
    hobby character varying(255),
    identity_type_id bigint NOT NULL,
    identity_no character varying(50) NOT NULL,
    email character varying(100) NOT NULL,
    phone_number1 character varying(50) NOT NULL,
    phone_number2 character varying(50),
    parent_phone_number character varying(50) NOT NULL,
    child_sequence character varying(5),
    how_many_brothers character varying(5),
    martial_status_id bigint NOT NULL,
    addbook_id bigint,
    token character varying(10),
    expired_token date,
    marriage_year character varying(10),
    company_id bigint NOT NULL,
    is_process boolean,
    is_complete boolean
);


ALTER TABLE public.x_biodata OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16595)
-- Name: x_biodata_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.x_biodata ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.x_biodata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 209 (class 1259 OID 16635)
-- Name: x_certification_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_certification_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.x_certification_type OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16633)
-- Name: x_certification_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.x_certification_type ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.x_certification_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 201 (class 1259 OID 16607)
-- Name: x_employee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_employee (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    is_idle boolean,
    is_ero boolean,
    is_user_client boolean,
    ero_email character varying(100)
);


ALTER TABLE public.x_employee OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16605)
-- Name: x_employee_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.x_employee ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.x_employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 213 (class 1259 OID 16649)
-- Name: x_empolyee_leave; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_empolyee_leave (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    employee_id bigint NOT NULL,
    reqular_quota integer NOT NULL,
    annual_collective_leave integer NOT NULL,
    leave_already_taken integer NOT NULL
);


ALTER TABLE public.x_empolyee_leave OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16647)
-- Name: x_empolyee_leave_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.x_empolyee_leave ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.x_empolyee_leave_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 211 (class 1259 OID 16642)
-- Name: x_empolyee_training; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_empolyee_training (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date,
    modified_by bigint,
    modified_on date,
    delete_on bigint,
    delete_by date,
    is_delete boolean NOT NULL,
    empolyee_id bigint NOT NULL,
    training_id bigint NOT NULL,
    training_organizer_id bigint,
    training_date date,
    training_type_id bigint,
    certification_type_id bigint,
    status character varying(15) NOT NULL
);


ALTER TABLE public.x_empolyee_training OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16640)
-- Name: x_empolyee_training_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.x_empolyee_training ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.x_empolyee_training_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 197 (class 1259 OID 16587)
-- Name: x_keahlian; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_keahlian (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on time with time zone NOT NULL,
    modified_by bigint,
    modified_on time with time zone,
    delete_by bigint,
    delete_on time with time zone,
    is_delete boolean NOT NULL,
    biodata_id bigint NOT NULL,
    skill_name character varying(100),
    skill_level_id bigint,
    notes character varying(1000)
);


ALTER TABLE public.x_keahlian OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16585)
-- Name: x_keahlian_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.x_keahlian ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.x_keahlian_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 203 (class 1259 OID 16614)
-- Name: x_training; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_training (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.x_training OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16612)
-- Name: x_training_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.x_training ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.x_training_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 205 (class 1259 OID 16621)
-- Name: x_training_organizer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_training_organizer (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    name character varying(100) NOT NULL,
    notes character varying(100) NOT NULL
);


ALTER TABLE public.x_training_organizer OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16619)
-- Name: x_training_organizer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.x_training_organizer ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.x_training_organizer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 207 (class 1259 OID 16628)
-- Name: x_training_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.x_training_type (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    delete_by bigint,
    delete_on date,
    is_delete boolean NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.x_training_type OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16626)
-- Name: x_training_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.x_training_type ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.x_training_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 2877 (class 0 OID 16597)
-- Dependencies: 199
-- Data for Name: x_biodata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_biodata (id, created_by, created_on, modified_by, modified_on, delete_by, delete_on, is_delete, fullname, nick_name, pob, dob, gender, religion_id, high, weight, nationality, ethnic, hobby, identity_type_id, identity_no, email, phone_number1, phone_number2, parent_phone_number, child_sequence, how_many_brothers, martial_status_id, addbook_id, token, expired_token, marriage_year, company_id, is_process, is_complete) FROM stdin;
1	1	09:21:26.911461+07	\N	\N	\N	\N	f	Mike Mangini	Mike	Paris	1990-08-26	t	1	\N	\N	\N	\N	\N	1	123456780	mike@gmail.com	088776655	\N	0874455321	\N	\N	1	\N	\N	\N	\N	1	\N	\N
2	1	09:22:53.500634+07	\N	\N	\N	\N	f	Toru	Toru	Japan	1992-07-12	t	1	\N	\N	\N	\N	\N	1	100990088	toru@gmail.com	081234443	\N	0897788665	\N	\N	1	\N	\N	\N	\N	1	\N	\N
3	1	09:23:46.598941+07	\N	\N	\N	\N	f	Ahmad Arip	Arif	Indonesia	1996-08-26	t	1	\N	\N	\N	\N	\N	1	908766522	arip@gmail.com	0896664321	\N	08877123009	\N	\N	1	\N	\N	\N	\N	1	\N	\N
4	1	09:24:31.69634+07	\N	\N	\N	\N	f	Angel	Angel	Indonesia	1994-01-05	t	1	\N	\N	\N	\N	\N	1	120891232	angel@gmail.com	0851218721	\N	081657822	\N	\N	1	\N	\N	\N	\N	1	\N	\N
5	1	09:25:11.379992+07	\N	\N	\N	\N	f	Miya	Miya	Indonesia	1995-12-16	t	1	\N	\N	\N	\N	\N	1	170817231	miya@gmail.com	08977462121	\N	0882163761	\N	\N	1	\N	\N	\N	\N	1	\N	\N
6	1	15:13:10.01346+07	\N	\N	\N	\N	f	Baby Kumal	Baby	Jakarta	1995-09-09	f	2	\N	\N	\N	\N	\N	1	123717232	baby@gmail.com	081231961	\N	078917283	\N	\N	1	\N	\N	\N	\N	2	\N	\N
7	1	16:23:00.70418+07	\N	\N	\N	\N	f	Angela	Angel	Yogyakarta	1993-09-09	f	2	\N	\N	\N	\N	\N	1	123717232	angela@gmail.com	097732812	\N	0718237	\N	\N	1	\N	\N	\N	\N	2	\N	\N
8	1	16:23:27.419277+07	\N	\N	\N	\N	f	Mio	Mio	Garut	1996-10-09	f	2	\N	\N	\N	\N	\N	1	12370187231	mio@gmail.com	097732812	\N	0718237	\N	\N	1	\N	\N	\N	\N	2	\N	\N
9	1	16:24:03.882651+07	\N	\N	\N	\N	f	Biru	Biru	Tasikmalaya	1991-10-09	f	2	\N	\N	\N	\N	\N	1	1381723	biru@gmail.com	08993216	\N	0883216	\N	\N	1	\N	\N	\N	\N	2	\N	\N
10	1	16:24:45.345907+07	\N	\N	\N	\N	f	Kaka	Kaka	Tangerang	1990-10-09	t	2	\N	\N	\N	\N	\N	1	1231822	kaka@gmail.com	0832662163	\N	0887216365	\N	\N	1	\N	\N	\N	\N	2	\N	\N
\.


--
-- TOC entry 2887 (class 0 OID 16635)
-- Dependencies: 209
-- Data for Name: x_certification_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_certification_type (id, created_by, created_on, modified_by, modified_on, delete_by, delete_on, is_delete, code, name) FROM stdin;
1	1	2020-05-23	\N	\N	\N	\N	f	ST-001	Sertifikasi Software Engginer
\.


--
-- TOC entry 2879 (class 0 OID 16607)
-- Dependencies: 201
-- Data for Name: x_employee; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_employee (id, created_by, created_on, modified_by, modified_on, delete_by, delete_on, is_delete, biodata_id, is_idle, is_ero, is_user_client, ero_email) FROM stdin;
1	1	2020-05-23	\N	\N	\N	\N	f	6	\N	\N	\N	\N
2	1	2020-05-23	\N	\N	\N	\N	f	7	\N	\N	\N	\N
3	1	2020-05-23	\N	\N	\N	\N	f	8	\N	\N	\N	\N
4	1	2020-05-23	\N	\N	\N	\N	f	9	\N	\N	\N	\N
5	1	2020-05-23	\N	\N	\N	\N	f	10	\N	\N	\N	\N
\.


--
-- TOC entry 2891 (class 0 OID 16649)
-- Dependencies: 213
-- Data for Name: x_empolyee_leave; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_empolyee_leave (id, created_by, created_on, modified_by, modified_on, delete_by, delete_on, is_delete, employee_id, reqular_quota, annual_collective_leave, leave_already_taken) FROM stdin;
1	1	2020-05-25	\N	\N	\N	\N	f	1	16	4	4
3	1	2020-05-25	\N	\N	\N	\N	f	3	16	4	4
4	1	2020-05-25	\N	\N	\N	\N	f	4	16	4	4
5	1	2020-05-25	\N	\N	\N	\N	f	5	16	4	4
2	1	2020-05-25	1	2020-05-25	\N	\N	f	2	16	4	4
6	1	2020-05-25	\N	\N	1	2020-05-25	t	1	12	2	4
\.


--
-- TOC entry 2889 (class 0 OID 16642)
-- Dependencies: 211
-- Data for Name: x_empolyee_training; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_empolyee_training (id, created_by, created_on, modified_by, modified_on, delete_on, delete_by, is_delete, empolyee_id, training_id, training_organizer_id, training_date, training_type_id, certification_type_id, status) FROM stdin;
1	1	2020-05-23	1	2020-05-24	\N	\N	f	1	1	1	2020-06-07	2	1	submitted
3	1	2020-05-23	1	2020-05-24	\N	\N	f	3	3	1	2020-05-11	2	1	submitted
4	1	2020-05-23	1	2020-05-24	\N	\N	f	4	4	1	2020-06-30	1	1	submitted
5	1	2020-05-23	1	2020-05-24	\N	\N	f	5	5	1	2020-07-27	2	1	submitted
2	1	2020-05-23	1	2020-05-24	\N	\N	f	2	2	1	2020-06-22	1	1	submitted
8	1	2020-05-26	\N	\N	1	2020-05-26	t	3	1	1	\N	\N	\N	submitted
\.


--
-- TOC entry 2875 (class 0 OID 16587)
-- Dependencies: 197
-- Data for Name: x_keahlian; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_keahlian (id, created_by, created_on, modified_by, modified_on, delete_by, delete_on, is_delete, biodata_id, skill_name, skill_level_id, notes) FROM stdin;
4	1	10:19:04.288211+07	\N	\N	\N	\N	f	1	PostgreSQL	2	\N
3	1	10:18:19.13619+07	1	11:12:24.968702+07	\N	\N	t	1	Python	2	null
5	1	14:19:49.799664+07	1	14:19:58.287833+07	\N	\N	f	1	MySQL	3	\N
6	1	14:20:50.150122+07	\N	\N	\N	\N	f	2	Golang	3	\N
7	1	14:21:05.602041+07	\N	\N	\N	\N	f	2	Python	2	\N
8	1	14:21:25.05555+07	\N	\N	\N	\N	f	4	Postgre	1	\N
9	1	14:21:36.435449+07	\N	\N	\N	\N	f	4	Java	3	\N
10	1	14:21:43.625477+07	\N	\N	\N	\N	f	5	Java	2	\N
11	1	14:21:52.302122+07	\N	\N	\N	\N	f	5	Javascript	3	\N
2	1	20:32:53.071308+07	1	14:12:36.361552+07	\N	\N	f	3	Java	1	\N
1	1	15:10:47.884954+07	1	14:11:57.814085+07	1	07:39:14.81532+07	f	3	Javascript	3	\N
\.


--
-- TOC entry 2881 (class 0 OID 16614)
-- Dependencies: 203
-- Data for Name: x_training; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_training (id, created_by, created_on, modified_by, modified_on, delete_by, delete_on, is_delete, code, name) FROM stdin;
1	1	2020-05-23	\N	\N	\N	\N	f	T-001	Postgre Sql
2	1	2020-05-23	\N	\N	\N	\N	f	T-002	OCP
3	1	2020-05-23	\N	\N	\N	\N	f	T-003	OCA
4	1	2020-05-23	\N	\N	\N	\N	f	T-004	PMO
5	1	2020-05-23	\N	\N	\N	\N	f	T-005	Golang
\.


--
-- TOC entry 2883 (class 0 OID 16621)
-- Dependencies: 205
-- Data for Name: x_training_organizer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_training_organizer (id, created_by, created_on, modified_by, modified_on, delete_by, delete_on, is_delete, name, notes) FROM stdin;
1	1	2020-05-23	\N	\N	\N	\N	f	Micro Academy Indonesia	Organizer Training
\.


--
-- TOC entry 2885 (class 0 OID 16628)
-- Dependencies: 207
-- Data for Name: x_training_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.x_training_type (id, created_by, created_on, modified_by, modified_on, delete_by, delete_on, is_delete, code, name) FROM stdin;
1	1	2020-05-23	\N	\N	\N	\N	f	TT-001	Bootcamp
2	1	2020-05-23	\N	\N	\N	\N	f	TT-002	Bootcamp Online
3	1	2020-05-23	\N	\N	\N	\N	t	ST-001	Sertifikasi Software Engginer
\.


--
-- TOC entry 2897 (class 0 OID 0)
-- Dependencies: 198
-- Name: x_biodata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_biodata_id_seq', 10, true);


--
-- TOC entry 2898 (class 0 OID 0)
-- Dependencies: 208
-- Name: x_certification_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_certification_type_id_seq', 1, true);


--
-- TOC entry 2899 (class 0 OID 0)
-- Dependencies: 200
-- Name: x_employee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_employee_id_seq', 5, true);


--
-- TOC entry 2900 (class 0 OID 0)
-- Dependencies: 212
-- Name: x_empolyee_leave_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_empolyee_leave_id_seq', 6, true);


--
-- TOC entry 2901 (class 0 OID 0)
-- Dependencies: 210
-- Name: x_empolyee_training_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_empolyee_training_id_seq', 8, true);


--
-- TOC entry 2902 (class 0 OID 0)
-- Dependencies: 196
-- Name: x_keahlian_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_keahlian_id_seq', 11, true);


--
-- TOC entry 2903 (class 0 OID 0)
-- Dependencies: 202
-- Name: x_training_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_training_id_seq', 5, true);


--
-- TOC entry 2904 (class 0 OID 0)
-- Dependencies: 204
-- Name: x_training_organizer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_training_organizer_id_seq', 1, true);


--
-- TOC entry 2905 (class 0 OID 0)
-- Dependencies: 206
-- Name: x_training_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.x_training_type_id_seq', 3, true);


--
-- TOC entry 2738 (class 2606 OID 16604)
-- Name: x_biodata x_biodata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_biodata
    ADD CONSTRAINT x_biodata_pkey PRIMARY KEY (id);


--
-- TOC entry 2748 (class 2606 OID 16639)
-- Name: x_certification_type x_certification_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_certification_type
    ADD CONSTRAINT x_certification_type_pkey PRIMARY KEY (id);


--
-- TOC entry 2740 (class 2606 OID 16611)
-- Name: x_employee x_employee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_employee
    ADD CONSTRAINT x_employee_pkey PRIMARY KEY (id);


--
-- TOC entry 2752 (class 2606 OID 16653)
-- Name: x_empolyee_leave x_empolyee_leave_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_empolyee_leave
    ADD CONSTRAINT x_empolyee_leave_pkey PRIMARY KEY (id);


--
-- TOC entry 2750 (class 2606 OID 16646)
-- Name: x_empolyee_training x_empolyee_training_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_empolyee_training
    ADD CONSTRAINT x_empolyee_training_pkey PRIMARY KEY (id);


--
-- TOC entry 2736 (class 2606 OID 16594)
-- Name: x_keahlian x_keahlian_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_keahlian
    ADD CONSTRAINT x_keahlian_pkey PRIMARY KEY (id);


--
-- TOC entry 2744 (class 2606 OID 16625)
-- Name: x_training_organizer x_training_organizer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_training_organizer
    ADD CONSTRAINT x_training_organizer_pkey PRIMARY KEY (id);


--
-- TOC entry 2742 (class 2606 OID 16618)
-- Name: x_training x_training_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_training
    ADD CONSTRAINT x_training_pkey PRIMARY KEY (id);


--
-- TOC entry 2746 (class 2606 OID 16632)
-- Name: x_training_type x_training_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.x_training_type
    ADD CONSTRAINT x_training_type_pkey PRIMARY KEY (id);


-- Completed on 2020-05-26 07:45:31

--
-- PostgreSQL database dump complete
--

