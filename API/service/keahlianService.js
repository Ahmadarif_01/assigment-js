module.exports = exports = (server, pool) => {
    server.get('/api/get_keahlian/:id', (req, res) => {
        const id = req.params.id;

        pool.query(`select * from x_keahlian where "is_delete" = 'false' and "biodata_id" = ${id}`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/get_perkeahlian/:id', (req, res) => {
        const id = req.params.id;

        pool.query(`select id, skill_name, skill_level_id, notes, biodata_id from x_keahlian where "is_delete" = 'false' and "id" = ${id}`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });


    server.get('/api/get_perbiodata/:id', (req, res) => {
        const id = req.params.id;

        pool.query(`select id from x_biodata where "is_delete" = 'false' and "id" = ${id}`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });


    server.put('/api/updateKeahlian/:id', (req, res) => {
        const id = req.params.id;

        console.log(req.body)
        const {
            skill_name,
            skill_level_id,
            notes,
        } = req.body;

        if (notes == null) {
            var query = `UPDATE x_keahlian
        SET modified_by = 1, modified_on = current_timestamp, skill_name = '${skill_name}', 
        skill_level_id = ${skill_level_id} 
        WHERE id = ${id};`
        } else if (notes == '') {
            var query = `UPDATE x_keahlian
        SET modified_by = 1, modified_on = current_timestamp, skill_name = '${skill_name}', 
        skill_level_id = ${skill_level_id} 
        WHERE id = ${id};`
        }
        else {
            var query = `UPDATE x_keahlian
        SET modified_by = 1, modified_on = current_timestamp, skill_name = '${skill_name}', 
        skill_level_id = ${skill_level_id} , notes= '${notes}'
        WHERE id = ${id};`
        }

        console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: {
                        detail: error.detail
                    }
                })
            } else {
                res.send(200, {
                    success: true,
                    result: "Berhasil Update Data"
                })
            }
        })
    })


    server.post('/api/Keahlian_post', (req, res) => {
        console.log(req.body)

        const {
            skill_name,
            skill_level_id,
            notes,
            biodata_id
        } = req.body;

        let qname = skill_name != "" ? ` '${skill_name}'` : null;
        let qnotes = notes != "" ? ` '${notes}' ` : null;
        let detQuery = `select id from x_biodata`;

        var query = `INSERT INTO public."x_keahlian"(
            created_by, created_on, is_delete, skill_name, skill_level_id, notes, biodata_id)
            values(1, current_timestamp, false, ${qname}, ${skill_level_id}, ${qnotes}, (${detQuery}))`;

        console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Disimpan"
                })
            }
        });


    });

    server.put('/api/deleteKeahlian/:id', (req, res) => {
        const id = req.params.id;

        var query = `UPDATE x_keahlian SET "is_delete" = 'true', "delete_by" = 1, "delete_on" = current_timestamp
        where "id" = ${id}`

        console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data Berhasil dihapus`
                })
            }
        })
    })

}