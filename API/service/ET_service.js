module.exports = exports = (server, pool) => {
    server.post('/api/get_ETraining', (req, res) => {
        const { search_nama, search_pelatihan, order, page, pagesize } = req.body;
        let qFilter = search_nama != "" ? ` AND a."fullname" LIKE '%${search_nama}%' ` : ``;
        let qFilterp = search_pelatihan != "" ? ` AND e."name" LIKE '%${search_pelatihan}'` : ``;
        let qOrder = order != "" ? `ORDER BY a."fullname" DESC` : `ORDER BY a."fullname"`;
        let perpage = (page - 1) * pagesize;

        let query = `select c."id", a."fullname", e."name" as nt, TO_CHAR(c."training_date" :: DATE, 'dd month yyyy'),
          d."name" as to, c."status" from x_biodata as a
          join x_employee as b on b.biodata_id = a.id
          join x_empolyee_training as c on c.empolyee_id = b.id
          join x_training as e on e.id = c.training_id
          join x_training_organizer as d on d.id = c.training_organizer_id
          where c."is_delete" = 'false' ${qFilter} ${qFilterp} ${qOrder} LIMIT ${pagesize} OFFSET ${perpage};`
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            if (result.rows.length > 0) {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            } else {
                res.send(400, {
                    success: false,
                    result: `Data tidak ditemukan`
                })
            }
        })
    });

    server.get('/api/get_CT', (req, res) => {

        pool.query(`select * from x_certification_type where is_delete = 'false'`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/get_T', (req, res) => {

        pool.query(`select * from x_training where is_delete = 'false'`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/get_TO', (req, res) => {

        pool.query(`select * from x_training_organizer where is_delete = 'false'`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/get_TT', (req, res) => {

        pool.query(`select * from x_training_type where is_delete = 'false'`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/get_karyawan', (req, res) => {

        pool.query(`select a.*, b."fullname" from x_employee as a
        join x_biodata as b on b.id = a.biodata_id where a."is_delete" = 'false'`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/get_ET/:id', (req, res) => {
        const id = req.params.id;

        pool.query(`Select * from x_empolyee_training where "is_delete" = 'false' and "id" = ${id}`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.put('/api/updateET/:id', (req, res) => {
        const id = req.params.id;

        console.log(req.body)
        const {
            empolyee_id,
            training_id,
            training_organizer_id,
            training_date,
            training_type_id,
            certification_type_id
        } = req.body;

        if (training_date == null) {
            var query = `UPDATE x_empolyee_training
            SET modified_by = 1, modified_on = current_timestamp, empolyee_id = ${ empolyee_id}, training_id = ${training_id}, training_organizer_id = ${training_organizer_id},
            training_type_id = ${training_type_id}, certification_type_id = ${certification_type_id}
            WHERE id = ${ id}; `
        } else {
            var query = `UPDATE x_empolyee_training
            SET modified_by = 1, modified_on = current_timestamp, empolyee_id = ${ empolyee_id}, training_id = ${training_id}, training_organizer_id = ${training_organizer_id},
            training_date = '${training_date}', training_type_id = ${training_type_id}, certification_type_id = ${certification_type_id}
            WHERE id = ${ id}; `
        }
        console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: {
                        detail: error.detail
                    }
                })
            } else {
                res.send(200, {
                    success: true,
                    result: "Berhasil Update Data"
                })
            }
        })
    })


    server.post('/api/hitung_ET', (req, res) => {
        const { order, page, pagesize } = req.body;

        var query = `select count("id") as totaldata 
        from "x_empolyee_training" where "is_delete" = false `;
        //console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })

    });


    server.put('/api/deleteET/:id', (req, res) => {
        const id = req.params.id;

        var query = `UPDATE "x_empolyee_training" SET "is_delete" = 'true', "delete_by" = current_timestamp, "delete_on" = 1
        where "id" = ${id}`

        console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data Berhasil dihapus`
                })
            }
        })
    })

    server.post('/api/ET_post', (req, res) => {
        console.log(req.body)

        const {
            empolyee_id,
            training_id,
            training_organizer_id,
            training_date,
            training_type_id,
            certification_type_id
        } = req.body;

        let qTO = training_organizer_id != "" ? ` ${training_organizer_id}` : null;
        let qDate = training_date != "" ? ` '${training_date}' ` : null;
        let qTT = training_type_id != "" ? `${training_type_id}` : null;
        let qCT = certification_type_id != "" ? `${certification_type_id}` : null;

        var query = `INSERT INTO public.x_empolyee_training(
            created_by, created_on, is_delete, empolyee_id, training_id, training_organizer_id, training_date,
            training_type_id, certification_type_id, status)
            values(1, current_timestamp, false, ${empolyee_id}, ${training_id}, ${qTO}, ${qDate},
            ${qTT}, ${qCT}, 'submitted')`;

        console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Disimpan"
                })
            }
        });


    });



}