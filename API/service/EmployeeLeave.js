module.exports = exports = (server, pool) => {

    server.post('/api/cari_employe', (req, res) => {
        const { search, order, page, pagesize } = req.body;
        let qFilter = search != "" ? ` AND b."fullname" LIKE '%${search}%' ` : ``;
        let qOrder = order != "" ? `ORDER BY b."fullname" DESC` : `ORDER BY b."fullname"`;
        let perpage = (page - 1) * pagesize;

        let query = `select p."id", b."fullname", p."reqular_quota" as reqular_quota, p."annual_collective_leave", p."leave_already_taken" from "x_biodata" as b
        join x_employee as k on b.id = k.biodata_id
        join x_empolyee_leave as p on k.id = p.employee_id where p."is_delete" = false  ${qFilter} ${qOrder} LIMIT ${pagesize} OFFSET ${perpage};`;
        //console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            if (result.rows.length > 0) {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            } else {
                res.send(400, {
                    success: false,
                    result: `Data tidak ditemukan`
                })
            }
        })

    })

    server.get('/api/get_EL', (req, res) => {

        pool.query(`select * from x_employee_leave where is_delete = 'false'`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    })


    server.get('/api/getEL/:id', (req, res) => {
        const id = req.params.id;

        let query = `select id, employee_id, reqular_quota, annual_collective_leave, leave_already_taken from x_empolyee_leave where id = '${id}' `
        //console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    })

    server.put('/api/updateEL/:id', (req, res) => {
        const id = req.params.id;
        const {
            employee_id,
            reqular_quota,
            annual_collective_leave,
            leave_already_taken
        } = req.body;

        var query = `UPDATE public.x_empolyee_leave
        SET modified_by = 1, modified_on = current_timestamp, employee_id = ${employee_id}, reqular_quota = ${reqular_quota}, 
        annual_collective_leave = ${annual_collective_leave}, leave_already_taken=${leave_already_taken}
        WHERE id = ${id};`

        console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: {
                        detail: error.detail
                    }
                })
            } else {
                res.send(200, {
                    success: true,
                    result: "Berhasil Update Data"
                })
            }
        })
    })


    server.post('/api/EL_post', (req, res) => {
        console.log(req.body)

        const {
            employee_id,
            reqular_quota,
            annual_collective_leave,
            leave_already_taken
        } = req.body;

        var query = `INSERT INTO public.x_empolyee_leave(
            created_by, created_on, is_delete, employee_id, reqular_quota, annual_collective_leave, leave_already_taken)
            values(1, current_timestamp, false, ${employee_id}, ${reqular_quota}, ${annual_collective_leave}, ${leave_already_taken})`;

        console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Disimpan"
                })
            }
        });


    });


    server.get('/api/get_karyawa', (req, res) => {

        pool.query(`select e."id", b."fullname" from x_biodata as b
        join x_employee as e on e.biodata_id = b.id where e."is_delete" = 'false'`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    })


    server.post('/api/hitung_EL', (req, res) => {
        const { order, page, pagesize } = req.body;

        var query = `select count(p."id") as totaldata 
        from "x_biodata" as b
        join x_employee as k on b.id = k.biodata_id
        join x_empolyee_leave as p on k.id = p.employee_id where p."is_delete" = false `;
        //console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })

    });


    server.put('/api/deleteL/:id', (req, res) => {
        const id = req.params.id;

        var query = `UPDATE x_empolyee_leave SET "is_delete" = 'true', "delete_by" = 1, "delete_on" = current_timestamp
        where "id" = ${id}`

        console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data Berhasil dihapus`
                })
            }
        })
    })


}