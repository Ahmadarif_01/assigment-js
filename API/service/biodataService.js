module.exports = exports = (server, pool) => {
    server.get('/api/get_biodata', (req, res) => {

          pool.query(`Select * from public."x_biodata" where "is_delete" = 'false' and "company_id" = '1' order by fullname`, (error, result) => {
                if (error) {
                      res.send(400, {
                            success: false,
                            result: error
                      })
                } else {
                      res.send(200, {
                            success: true,
                            result: result.rows
                      })
                }
          })
    });


    server.get('/api/get_bio/:id', (req, res) => {
      const id = req.params.id;

      pool.query(`Select * from public."x_biodata" where "is_delete" = 'false' and "id" = ${id}`, (error, result) => {
          if (error) {
              res.send(400, {
                  success: false,
                  result: error
              })
          } else {
              res.send(200, {
                  success: true,
                  result: result.rows
              })
          }
      })
  });

}