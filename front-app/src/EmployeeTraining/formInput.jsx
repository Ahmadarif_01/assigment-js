import React from 'react';
import Modal from 'react-bootstrap/Modal';
import 'bootstrap/dist/css/bootstrap.min.css';


class FormInput extends React.Component {
    render() {
        const { open_modal, mode, m_pelatihan, cancel, CT_option,
            T_option, TO_option, TT_option, k_option,
            selectHandler_employee, selectHandler_to, selectHandler_tt,
            selectHandler_ct, changeHandler_td, selectHandler_training, simpan, errors } = this.props;
        return (
            <Modal show={open_modal} style={{ opacity: 1 }}>
                <Modal.Header style={{ backgroundColor: 'lightblue' }}>
                    <Modal.Title>{mode === 'create' ? <label>Tambah Data Pelatihan Karyawan</label> : <label>Edit Data Pelatihan Karyawan</label>}</Modal.Title>
                </Modal.Header>
                {/* {JSON.stringify(m_pelatihan)}<br /> */}

                <Modal.Body>
                    <form>
                        <div class="row">
                            <div class="col">
                                <small>Karyawan *</small>
                                <select class='form-control' onChange={selectHandler_employee('empolyee_id')}>
                                    <option disabled selected>-- Pilih Karyawan --</option>
                                    {
                                        k_option.map(data => {
                                            return (
                                                <option value={data.id} selected={m_pelatihan.empolyee_id === data.id}>{data.fullname}</option>
                                            )
                                        })
                                    }
                                </select>
                                <span style={{ color: "red" }}>{errors["empolyee_id"]}</span>
                                <br />
                                <small>Penyelenggara</small>
                                <select class='form-control' onChange={selectHandler_to('training_organizer_id')}>
                                    <option disabled selected>-- Pilih Penyelenggara --</option>
                                    {
                                        TO_option.map(data => {
                                            return (
                                                <option value={data.id} selected={m_pelatihan.training_organizer_id === data.id}>{data.name}</option>
                                            )
                                        })
                                    }
                                </select>
                                <br />
                                <small>Jenis Pelatihan</small>
                                <select class='form-control' onChange={selectHandler_tt('training_type_id')}>
                                    <option disabled selected>-- Pilih Jenis Pelatihan --</option>
                                    {
                                        TT_option.map(data => {
                                            return (
                                                <option value={data.id} selected={m_pelatihan.training_type_id === data.id}>{data.name}</option>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                            <div class="col">
                                <small>Pelatihan *</small>
                                <select class='form-control' onChange={selectHandler_training('training_id')}>
                                    <option disabled selected>-- Pilih Pelatihan --</option>
                                    {
                                        T_option.map(data => {
                                            return (
                                                <option value={data.id} selected={m_pelatihan.training_id === data.id}>{data.name}</option>
                                            )
                                        })
                                    }
                                </select>
                                <span style={{ color: "red" }}>{errors["training_id"]}</span>
                                <br />
                                <small>Tg.Pelatihan</small>
                                <input type='date' class='form-control' onChange={changeHandler_td('training_date')} />
                                <br />
                                <small>Jenis Sertifikasi</small>
                                <select class='form-control' onChange={selectHandler_ct('certification_type_id')}>
                                    <option disabled selected>-- Pilih Jenis Sertifikasi --</option>
                                    {
                                        CT_option.map(data => {
                                            return (
                                                <option value={data.id} selected={m_pelatihan.certification_type_id === data.id}>{data.name}</option>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <div class='btn-group'>
                        <button class='btn btn-danger' onClick={cancel}>Batal</button>
                        <button class='btn btn-primary' onClick={simpan} >Simpan</button>
                    </div>
                </Modal.Footer>
            </Modal>

        )
    }
}

export default FormInput;