import React from 'react';
import Table from 'react-bootstrap/Table';
import ET from '../Service/ET_service';
import FormInput from './formInput';
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import Modal from 'react-bootstrap/Modal';

class Employee_training extends React.Component {

    m_pelatihan = {
        empolyee_id: '',
        training_id: '',
        training_organizer_id: '',
        training_date: '',
        training_type_id: '',
        certification_type_id: ''
    }

    constructor() {
        super();
        this.state = {
            open_modal: false,
            open_delete: false,
            list_ET: [],
            mode: '',
            CT_option: [],
            T_option: [],
            TO_option: [],
            TT_option: [],
            k_option: [],
            Training_option: [],
            filter: {
                search_nama: '',
                search_pelatihan: '',
                order: '',
                page: '1',
                pagesize: '10'
            },
            totaldata: 1,
            errors:{}
        }
    }

    load_list = async (filter) => {
        const respon = await ET.getAll(filter);
        const countdata = await ET.countData(filter);
        if (respon.success) {
            this.setState({
                list_ET: respon.result,
                totaldata: Math.ceil(countdata.result[0].totaldata / filter.pagesize)
            })
        }
    }

    cancel = () => {
        this.setState({
            open_modal: false
        })
    }

    selectHandler_employee = name => ({ target: { value } }) => {
        this.setState({
            m_pelatihan: {
                ...this.state.m_pelatihan,
                [name]: value
            }
        })
    }

    selectHandler_training = name => ({ target: { value } }) => {
        this.setState({
            m_pelatihan: {
                ...this.state.m_pelatihan,
                [name]: value
            }
        })
    }

    selectHandler_to = name => ({ target: { value } }) => {
        this.setState({
            m_pelatihan: {
                ...this.state.m_pelatihan,
                [name]: value
            }
        })
    }

    selectHandler_tt = name => ({ target: { value } }) => {
        this.setState({
            m_pelatihan: {
                ...this.state.m_pelatihan,
                [name]: value
            }
        })
    }

    selectHandler_ct = name => ({ target: { value } }) => {
        this.setState({
            m_pelatihan: {
                ...this.state.m_pelatihan,
                [name]: value
            }
        })
    }

    changeHandler_td = name => ({ target: { value } }) => {
        this.setState({
            m_pelatihan: {
                ...this.state.m_pelatihan,
                [name]: value
            }
        })

    }

    componentDidMount() {
        const { filter } = this.state;
        this.load_list(filter);
        this.getTraining();
    }


    componentDidUpdate() {
        const { filter } = this.state;
        this.load_list(filter);
        // this.getTraining();
    }

    edit_data = async (id) => {
        const respon = await ET.getdatabyid(id);
        this.getCT();
        this.getT();
        this.getTO();
        this.getTT();
        this.getkaryawan();
        if (respon.success) {
            this.setState({
                mode: 'edit',
                open_modal: true,
                m_pelatihan: respon.result[0]
            })
        }
    }

    hendleOpen = () => {
        this.getCT();
        this.getT();
        this.getTO();
        this.getTT();
        this.getkaryawan();
        this.setState({
            open_modal: true,
            mode: 'create',
            m_pelatihan: {
                empolyee_id: '',
                training_id: '',
                training_organizer_id: '',
                training_date: '',
                training_type_id: '',
                certification_type_id: ''
            },
            errors:{}
        })
    }

    hendleValidation = () => {
        let fields = this.state.m_pelatihan;
        let errors = {};
        let formIsValid = true;

        
        if (!fields['empolyee_id']) {
            formIsValid = false;
            errors['empolyee_id'] = 'Jangan Kosong !';
        }

        
        if (!fields['training_id']) {
            formIsValid = false;
            errors['training_id'] = 'Jangan Kosong !';
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    getCT = async () => {
        const respon = await ET.getCT();
        this.setState({
            CT_option: respon.result
        })
    }

    getT = async () => {
        const respon = await ET.getT();
        this.setState({
            T_option: respon.result
        })
    }

    getTraining = async () => {
        const respon = await ET.getT();
        this.setState({
            Training_option: respon.result
        })
    }

    getTO = async () => {
        const respon = await ET.getTO();
        this.setState({
            TO_option: respon.result
        })
    }

    getTT = async () => {
        const respon = await ET.getTT();
        this.setState({
            TT_option: respon.result
        })
    }

    getkaryawan = async () => {
        const respon = await ET.getkaryawan();
        this.setState({
            k_option: respon.result
        })
    }

    simpan = async () => {
        const { m_pelatihan, mode } = this.state;

        if (mode === 'create') {

            if (this.hendleValidation()) {
                const respon = await ET.post(m_pelatihan);
                if (respon.success) {
                    alert("Data Berhasil Disimpan")
                    this.setState({
                        open_modal: false,
                        filter: {
                            search_nama: '',
                            search_pelatihan: '',
                            order: '',
                            page: '1',
                            pagesize: '10'
                        }
                    });
                }
            }
        } else {
            const respon = await ET.updateData(m_pelatihan);
            if (respon.success) {
                alert(respon.result);
            }
            else {
                alert(respon.result);
            }
            this.setState({
                open_modal: false
            })
        }
    }

    changeHandler = name => ({ target: { value } }) => {
        this.setState({
            filter: {
                ...this.state.filter,
                [name]: value
            }
        })

    }

    hendle_remove = () => {
        document.getElementById("cari").value = '';
        document.getElementById("cari_pelatihan").value = 'Cari Pelatihan';
        this.setState({
            filter: {
                search_nama: '',
                search_pelatihan: '',
                order: '',
                page: '1',
                pagesize: '10'
            }
        })
    }


    handlerSorting = () => {
        let order = "";
        const { filter } = this.state;
        if (filter.order === "") {
            order = "DESC"
        }
        this.setState({
            filter: {
                ...this.state.filter,
                ["order"]: order
            }
        })
    }

    pageSizeHandler = (val) => {
        this.setState({
            filter: {
                ...this.state.filter,
                ["pagesize"]: val
            }
        });
    }

    onChangePage = (number) => {
        const { filter } = this.state;
        this.setState({
            filter: {
                ...this.state.filter,
                ["page"]: number
            }
        });
    }

    renderPagination() {
        let items = [];
        const { filter, totaldata } = this.state;
        for (let number = 1; number <= totaldata; number++) {
            items.push(
                <PaginationItem key={number} active={number === filter.page}>
                    <PaginationLink onClick={() => this.onChangePage(number)} next>
                        {number}
                    </PaginationLink>
                </PaginationItem>
            );
        }
        return (
            <Pagination>{items}</Pagination>
        );
    }


    hendlerDel = async (id) => {
        const respon = await ET.getdatabyid(id);
        if (respon.success) {
            this.setState({
                open_delete: true,
                m_pelatihan: respon.result[0]
            })
        }
        else {
            alert(respon.result);
        }

    }

    cancel_delete = () => {
        this.setState({
            open_delete: false
        });
    }

    sureDelete = async (item) => {
        const { m_pelatihan } = this.state;
        const respon = await ET.delete_ET(m_pelatihan);

        if (respon.success) {
            alert('Sukses' + respon.result)
        }
        else {
            alert('error' + respon.result);
        }
        this.setState({
            open_delete: false,
            filter: {
                search_nama: '',
                search_pelatihan: '',
                order: '',
                page: '1',
                pagesize: '10'
            }
        })
    }

    render() {
        const { open_modal, m_pelatihan, list_ET, mode, CT_option,
            T_option,
            TO_option,
            TT_option,
            k_option, filter, Training_option, open_delete, errors } = this.state;
        return (
            <div>
                <h3>Employee Training</h3>
                <br />
                <FormInput open_modal={open_modal} mode={mode}
                    cancel={this.cancel} m_pelatihan={m_pelatihan}
                    CT_option={CT_option} T_option={T_option}
                    TO_option={TO_option} TT_option={TT_option}
                    k_option={k_option} selectHandler_employee={this.selectHandler_employee}
                    selectHandler_to={this.selectHandler_to} selectHandler_tt={this.selectHandler_tt}
                    selectHandler_ct={this.selectHandler_ct} changeHandler_td={this.changeHandler_td}
                    selectHandler_training={this.selectHandler_training} simpan={this.simpan}
                    errors={errors} />
                <div class="btn-group">

                    <input type='text' id="cari" placeholder='Cari Karyawan' class="form-control mb-2 mr-sm-2" onChange={this.changeHandler('search_nama')} />
                    <select id='cari_pelatihan' class="form-control mb-2 mr-sm-2" onChange={this.changeHandler('search_pelatihan')}>
                        <option disabled selected>Cari Pelatihan</option>
                        {
                            Training_option.map(data => {
                                return (
                                    <option value={data.name}>{data.name}</option>
                                )
                            })
                        }
                    </select>
                    <button class="btn btn-primary mb-2" onClick={this.hendle_remove}><i class='fa fa-search'></i></button>
                </div>
                <div class='btn-group pull-right'>
                    <button type='button' class='btn btn-info pull-right' onClick={this.handlerSorting}><i class="fa fa-sort-alpha-asc"></i></button>
                    <button type="button" class="btn btn-danger dropdown-toogle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-navicon"></i>
                    </button>

                    <ul class="dropdown-menu">
                        <li><a href="#" onClick={() => this.pageSizeHandler('5')}>5</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('10')}>10</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('20')}>20</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('30')}>30</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('40')}>40</a></li>
                    </ul>


                    <button type='button' class='btn btn-primary pull-right' onClick={this.hendleOpen} ><i class="fa fa-plus-circle"></i></button>
                </div>
                {/* {JSON.stringify(filter)}<br /> */}
                <Modal show={open_delete} style={{ opacity: 1 }}>
                    <Modal.Body>
                        Apakah anda akan menghapus data ini ?
                        </Modal.Body>
                    <Modal.Footer>
                        <div class='btn-group'>
                            <button class='btn btn-success' onClick={() => this.sureDelete(m_pelatihan.id)}>Ya</button>
                            <button class='btn btn-info' onClick={this.cancel_delete}>Tidak</button>
                        </div>
                    </Modal.Footer>
                </Modal>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>KARYAWAN</th>
                            <th>NAMA TRAINING</th>
                            <th>TGL.TRAINING</th>
                            <th>PENYELENGGARA</th>
                            <th>STATUS</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            list_ET.map(data => {
                                return (
                                    <tr>
                                        <td>{data.fullname}</td>
                                        <td>{data.nt}</td>
                                        <td>{data.to_char}</td>
                                        <td>{data.to}</td>
                                        <td>{data.status}</td>
                                        <td>
                                            <div class='btn-group'>
                                                <button type="button" class="btn btn-info dropdown-toogle"
                                                    data-toggle="dropdown" aria-expanded="false">
                                                    <small>More </small><i class="fa fa-sort-desc"></i>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" onClick={() => this.edit_data(data.id)}>Ubah</a></li>
                                                    <li><a href="#">Persetujuan</a></li>
                                                    <li><a href="#">Selesai</a></li>
                                                    <li><a href="#" onClick={() => this.hendlerDel(data.id)}>Hapus</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                        }

                    </tbody>

                </Table>
                <div class="pull-right">
                    {this.renderPagination()}
                </div>
            </div>
        )
    }
}
export default Employee_training;