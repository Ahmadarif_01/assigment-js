import axios from 'axios';
import { config } from '../Config/config';


export const ET = {

    getAll: (filter) => {
        const result = axios.post(config.apiUrl + '/get_ETraining', filter)

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        // //console.log(result);
        return result;
    },

    getCT: () => {
        const result = axios.get(config.apiUrl + '/get_CT')

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        // //console.log(result);
        return result;
    },

    getT: () => {
        const result = axios.get(config.apiUrl + '/get_T')

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        // //console.log(result);
        return result;
    },

    getTO: () => {
        const result = axios.get(config.apiUrl + '/get_TO')

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        // //console.log(result);
        return result;
    },

    getTT: () => {
        const result = axios.get(config.apiUrl + '/get_TT')

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        // //console.log(result);
        return result;
    },

    getkaryawan: () => {
        const result = axios.get(config.apiUrl + '/get_karyawan')

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        // //console.log(result);
        return result;
    },


    getdatabyid: (id) => {
        const result = axios.get(config.apiUrl + '/get_ET/' + id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    updateData: (item) => {
        const result = axios.put(config.apiUrl + '/updateKeahlian/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    updateData: (item) => {
        const result = axios.put(config.apiUrl + '/updateET/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    countData: (filter) => {
        const result = axios.post(config.apiUrl + '/hitung_ET', filter)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    delete_ET: (item) => {
        const result = axios.put(config.apiUrl + '/deleteET/' + item.id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    post: (item) => {
        const result = axios.post(config.apiUrl + '/ET_post', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

}


export default ET;