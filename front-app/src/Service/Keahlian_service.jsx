import axios from 'axios';
import { config } from '../Config/config';


export const keahlian = {
    getdatabyid: (id) => {
        const result = axios.get(config.apiUrl + '/get_keahlian/' + id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getdatabyidkeahlian: (id) => {
        const result = axios.get(config.apiUrl + '/get_perkeahlian/' + id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getdatabyidbiodata: (id) => {
        const result = axios.get(config.apiUrl + '/get_perbiodata/' + id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    updateData: (item) => {
        const result = axios.put(config.apiUrl + '/updateKeahlian/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    post: (item) => {
        const result = axios.post(config.apiUrl + '/Keahlian_post', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    delete_keahlian: (item) => {
        const result = axios.put(config.apiUrl + '/deleteKeahlian/' + item.id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

}


export default keahlian;