import axios from 'axios';
import { config } from '../Config/config';


export const pelamar = {
    getAll: () => {
        const result = axios.get(config.apiUrl + '/get_biodata')
    
          .then(respons => {
            return {
              success: respons.data.success,
              result: respons.data.result
            }
          })
          .catch(error => {
            return {
              success: false,
              result: error
            }
          });
        // //console.log(result);
        return result;
      },


      getdatabyid: (id) => {
        const result = axios.get(config.apiUrl + '/get_bio/' + id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    }
}


export default pelamar;