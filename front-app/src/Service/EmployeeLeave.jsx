import axios from 'axios';
import { config } from '../Config/config';


export const EmployeeLeave = {
    getdatabyfn: (filter) => {

        const result = axios.post(config.apiUrl + '/cari_employe', filter)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getKaryawan: () => {
        const result = axios.get(config.apiUrl + '/get_karyawa')

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    getEL: () => {
        const result = axios.get(config.apiUrl + '/get_EL')

            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    getdatabyid: (id) => {
        const result = axios.get(config.apiUrl + '/getEL/' + id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    updateData: (item) => {
        const result = axios.put(config.apiUrl + '/updateEL/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    post: (item) => {
        const result = axios.post(config.apiUrl + '/EL_post', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    countData: (filter) => {
        const result = axios.post(config.apiUrl + '/hitung_EL', filter)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },


    delete_EL: (item) => {
        const result = axios.put(config.apiUrl + '/deleteL/' + item.id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    }


}




export default EmployeeLeave;