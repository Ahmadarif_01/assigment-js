import React from 'react';
import Table from 'react-bootstrap/Table';
import FormInput from './formInput';
import EmployeeLeave from '../Service/EmployeeLeave';
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import Modal from 'react-bootstrap/Modal';


class Employe extends React.Component {

    m_employe = {
        employee_id: '',
        reqular_quota: '',
        annual_collective_leave: '',
        leave_already_taken: ''
    }

    constructor() {
        super();
        this.state = {
            open_modal: false,
            open_delete: false,
            m_employe: this.m_employe,
            List_employe: [],
            p_option: [],
            mode: '',
            errors: {},
            filter: {
                search: '',
                order: '',
                page: '1',
                pagesize: '10'
            },
            totaldata: 1
        }

    }

    componentDidMount() {
        const { filter } = this.state
        this.loadList(filter);
    }

    componentDidUpdate() {
        const { filter } = this.state
        this.loadList(filter);
    }

    loadList = async (filter) => {
        const respon = await EmployeeLeave.getdatabyfn(filter);
        const countdata = await EmployeeLeave.countData(filter);
        if (respon.success) {
            this.setState({
                List_employe: respon.result,
                totaldata: Math.ceil(countdata.result[0].totaldata / filter.pagesize)
            })
        }
    }

    cancel_tambah = () => {
        this.setState({
            open_modal: false
        });
    }

    cancel_delete = () => {
        this.setState({
            open_delete: false
        });
    }


    hendlerEdit = async (id) => {
        const respon = await EmployeeLeave.getdatabyid(id);
        this.getOptionkaryawan();
        if (respon.success) {
            this.setState({
                mode: 'edit',
                open_modal: true,
                m_employe: respon.result[0]
            })
        }
        else {
            alert(respon.result);
        }
        this.setState({
            errors: {}
        })
    }

    getOptionkaryawan = async () => {
        const respon = await EmployeeLeave.getKaryawan();
        this.setState({
            p_option: respon.result
        })
    }

    selectHandler_karyawan = name => ({ target: { value } }) => {
        this.setState({
            m_employe: {
                ...this.state.m_employe,
                [name]: value
            }
        })
    }

    changeHandler_employe = name => ({ target: { value } }) => {
        this.setState({
            m_employe: {
                ...this.state.m_employe,
                [name]: value
            }
        })

    }

    changeHandler = name => ({ target: { value } }) => {
        this.setState({
            filter: {
                ...this.state.filter,
                [name]: value
            }
        })

    }


    onSave = async () => {
        const { m_employe, mode } = this.state;

        if (mode === 'create') {

            if (this.hendleValidation()) {
                const respon = await EmployeeLeave.post(m_employe);
                if (respon.success) {
                    alert("Data Berhasil Disimpan")
                    this.setState({
                        open_modal: false,
                        filter: {
                            search: '',
                            order: '',
                            page: '1',
                            pagesize: '10'
                        }
                    });
                }
            }
        }
        else {
            if (this.hendleValidation()) {
                const respon = await EmployeeLeave.updateData(m_employe);
                if (respon.success) {
                    alert(respon.result)
                }
                else {
                    alert('Error : ' + respon.result)
                }
                this.setState({
                    open_modal: false,
                    filter: {
                        search: '',
                        order: '',
                        page: '1',
                        pagesize: '10'
                    },
                })
            }
        }

    }

    hendleValidation = () => {
        let fields = this.state.m_employe;
        let errors = {};
        let formIsValid = true;

        //karyawan
        if (!fields['employee_id']) {
            formIsValid = false;
            errors['employee_id'] = 'Jangan Kosong !';
        }

        //kuota reguler
        if (!fields['reqular_quota']) {
            formIsValid = false;
            errors['reqular_quota'] = 'Jangan Kosong !';
        }

        //cuti bersama
        if (!fields['annual_collective_leave']) {
            formIsValid = false;
            errors['annual_collective_leave'] = 'Jangan Kosong !';
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    hendle_remove = () => {
        document.getElementById("cari").value = ''
        this.setState({
            filter: {
                search: '',
                order: '',
                page: '1',
                pagesize: '10'
            }
        })
    }

    handlerSorting = () => {
        let order = "";
        const { filter } = this.state;
        if (filter.order === "") {
            order = "DESC"
        }
        this.setState({
            filter: {
                ...this.state.filter,
                ["order"]: order
            }
        })
    }

    pageSizeHandler = (val) => {
        this.setState({
            filter: {
                ...this.state.filter,
                ["pagesize"]: val
            }
        });
    }

    hendleOpen = () => {
        this.getOptionkaryawan();
        this.setState({
            open_modal: true,
            mode: 'create',
            m_employe: { leave_already_taken: '4' },
            errors: {}
        })
    }

    onChangePage = (number) => {
        const { filter } = this.state;
        this.setState({
            filter: {
                ...this.state.filter,
                ["page"]: number
            }
        });
    }

    renderPagination() {
        let items = [];
        const { filter, totaldata } = this.state;
        for (let number = 1; number <= totaldata; number++) {
            items.push(
                <PaginationItem key={number} active={number === filter.page}>
                    <PaginationLink onClick={() => this.onChangePage(number)} next>
                        {number}
                    </PaginationLink>
                </PaginationItem>
            );
        }
        return (
            <Pagination>{items}</Pagination>
        );
    }


    hendlerDel = async (id) => {
        const respon = await EmployeeLeave.getdatabyid(id);
        if (respon.success) {
            this.setState({
                open_delete: true,
                m_employe: respon.result[0]
            })
        }
        else {
            alert(respon.result);
        }

    }

    sureDelete = async (item) => {
        const { m_employe } = this.state;
        const respon = await EmployeeLeave.delete_EL(m_employe);

        if (respon.success) {
            alert('Sukses' + respon.result)
        }
        else {
            alert('error' + respon.result);
        }
        this.setState({
            open_delete: false,
            filter: {
                search: '',
                order: '',
                page: '1',
                pagesize: '10'
            }
        })
    }


    render() {
        const { open_modal, List_employe,
            p_option, m_employe, mode, errors, filter, open_delete } = this.state;
        return (
            <div>
                <h3>Employee Leave</h3>
                <br />
                {/* {JSON.stringify(filter)}<br /> */}
                {/* {JSON.stringify(List_employe)}<br /> */}
                <div class='btn-group pull-right'>
                    <button type='button' class='btn btn-info pull-right' onClick={this.handlerSorting}><i class="fa fa-sort-alpha-asc"></i></button>
                    <button type="button" class="btn btn-danger dropdown-toogle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-navicon"></i>
                    </button>

                    <ul class="dropdown-menu">
                        <li><a href="#" onClick={() => this.pageSizeHandler('5')}>5</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('10')}>10</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('20')}>20</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('30')}>30</a></li>
                        <li><a href="#" onClick={() => this.pageSizeHandler('40')}>40</a></li>
                    </ul>

                    <button type='button' class='btn btn-primary pull-right' onClick={this.hendleOpen}><i class="fa fa-plus-circle"></i></button>

                </div>

                <FormInput open_modal={open_modal} mode={mode}
                    cancel_tambah={this.cancel_tambah} p_option={p_option}
                    m_employe={m_employe} selectHandler_karyawan={this.selectHandler_karyawan}
                    changeHandler_employe={this.changeHandler_employe} errors={errors} onSave={this.onSave} />

                <div class="btn-group">

                    <input type='text' id="cari" placeholder='Cari Berdasarkan Karyawan' class="form-control mb-2 mr-sm-2" onChange={this.changeHandler('search')} />
                    <button class="btn btn-primary mb-2" title='Reset Pencarian' onClick={this.hendle_remove} ><i class='fa fa-search'></i></button>

                </div>

                <Modal show={open_delete} style={{ opacity: 1 }}>
                    <Modal.Body>
                        Apakah anda akan menghapus data ini ?
                        </Modal.Body>
                    <Modal.Footer>
                        <div class='btn-group'>
                            <button class='btn btn-success' onClick={() => this.sureDelete(m_employe.id)}>Ya</button>
                            <button class='btn btn-info' onClick={this.cancel_delete}>Tidak</button>
                        </div>
                    </Modal.Footer>
                </Modal>

                <div>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Karyawan</th>
                                <th>Kuota Reguler</th>
                                <th>Tahun Lalu</th>
                                <th>Cuti Bersama</th>
                                <th>Sudah Diambil</th>
                                <th>Sisa Cuti</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody >
                            {
                                List_employe.map(data => {
                                    return (
                                        <tr>
                                            <td>{data.fullname}</td>
                                            <td>{data.reqular_quota}</td>
                                            <td>
                                                {
                                                    parseInt(data.reqular_quota) - parseInt(data.annual_collective_leave) - parseInt(data.leave_already_taken)
                                                }
                                            </td>
                                            <td>{data.annual_collective_leave}</td>
                                            <td>{data.leave_already_taken}</td>
                                            <td>
                                                {
                                                    ((parseInt(data.reqular_quota) - parseInt(data.annual_collective_leave) - parseInt(data.leave_already_taken)) + parseInt(data.reqular_quota))
                                                    - (parseInt(data.annual_collective_leave) + parseInt(data.leave_already_taken))
                                                }
                                            </td>
                                            <td>
                                                <div class='btn-group'>
                                                    <button type="button" class="btn btn-info dropdown-toogle" data-toggle="dropdown" aria-expanded="false">
                                                        <small>More </small><i class="fa fa-sort-desc"></i>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onClick={() => this.hendlerEdit(data.id)}>Edit</a></li>
                                                        <li><a href="#" onClick={() => this.hendlerDel(data.id)}>Hapus</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })

                            }

                        </tbody>

                    </Table>
                    <div class="pull-right">
                        {this.renderPagination()}
                    </div>
                </div>

            </div>
        )
    }
}
export default Employe;