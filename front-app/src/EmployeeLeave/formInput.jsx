import React from 'react';
import Modal from 'react-bootstrap/Modal';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';

class FormInput extends React.Component {

    render() {
        const { open_modal, cancel_tambah, p_option, mode, m_employe
            , selectHandler_karyawan, changeHandler_employe, errors, onSave} = this.props;

        return (
            <Modal show={open_modal} style={{ opacity: 1 }}>
                <Modal.Header style={{ backgroundColor: 'lightblue' }}>
                    <Modal.Title >Atur Cuti Karyawan</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {/* {JSON.stringify(m_employe)}<br /> */}
                    <form>
                        <div class="row">
                            <div class="col">
                                <small>Karyawan *</small>
                                <select class="form-control" onChange={selectHandler_karyawan('employee_id')}>
                                    <option disabled selected>-Pilih Karyawan-</option>
                                    {
                                        p_option.map(data => {
                                            return (
                                                <option value={data.id} selected={m_employe.employee_id == data.id}>{data.fullname}</option>
                                            )
                                        })
                                    }
                                </select>
                                <span style={{ color: "red" }}>{errors["employee_id"]}</span>
                            </div>
                            <div class="col">
                                <small>Kuota Reguler *</small>
                                <input type="text" id='regular' value={m_employe.reqular_quota} onkeyup="SCTL();" class="form-control" onChange={changeHandler_employe('reqular_quota')} />
                                <span style={{ color: "red" }}>{errors["reqular_quota"]}</span>
                            </div>
                        </div>

                        <br />

                        <div class="row">
                            <div class="col">
                                <small>Total Cuti Bersama *</small>
                                <input type="text" id='cuti' value={m_employe.annual_collective_leave} class="form-control" onkeyup="SCTL();" onChange={changeHandler_employe('annual_collective_leave')} />
                                <span style={{ color: "red" }}>{errors["annual_collective_leave"]}</span>
                            </div>
                            <div class="col">
                                <small>Sisa Cuti Tahun Lalu</small>
                                <fieldset disabled>
                                    <input type="text" id="SCTL" value={mode == 'create' ? '0':parseInt(m_employe.reqular_quota) - parseInt(m_employe.annual_collective_leave) - parseInt(m_employe.leave_already_taken)} class="form-control" placeholder='0' />
                                </fieldset>
                            </div>
                        </div>

                        <br />

                        <div class="row">
                            <div class="col">
                                <fieldset disabled>
                                    <small>Jml. Cuti Sudah Diambil</small>
                                    <input type="text" id='sudah_cuti' value={mode == 'create' ? '0':m_employe.leave_already_taken} class="form-control" placeholder='0' />
                                </fieldset>
                            </div>
                            <div class="col">
                                <fieldset disabled>
                                    <small>Total Sisa Cuti</small>
                                    <input type="text" class="form-control" value={mode == 'create' ? '0':((parseInt(m_employe.reqular_quota) - parseInt(m_employe.annual_collective_leave) - parseInt(m_employe.leave_already_taken)) + parseInt(m_employe.reqular_quota))
                                    - (parseInt(m_employe.annual_collective_leave) + parseInt(m_employe.leave_already_taken))} placeholder='0' />
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <div class='btn-group'>
                        <Button variant='danger' onClick={cancel_tambah}>Batal</Button>
                        <Button variant='primary' onClick={onSave}>Simpan</Button>
                    </div>
                </Modal.Footer>
            </Modal>

        )
    }

}

export default FormInput