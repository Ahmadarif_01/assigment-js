import React from 'react';
import Sidebar from '../Layout/Sidebar';
import Header from '../Layout/Header';
import {Route, Switch } from 'react-router-dom';
import Pelamar from '../Pelamar';
import ET from '../EmployeeTraining';
import EL from '../EmployeeLeave';


function App() {
    return (
      <div className="wrapper">
        <Header />
        <Sidebar />
        <div className="content-wrapper">
  
          <section className="content">
            <Switch>
              <Route exact path="/Pelamar" component={Pelamar} />
              <Route exact path="/ET" component={ET} />
              <Route exact path="/EL" component={EL} />
            </Switch>
            </section>
        </div>
      </div>
    )
  }
  
  export default App;
  