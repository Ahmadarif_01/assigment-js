import React from 'react';

export default class Header extends React.Component {
    render() {
        return (
            <header class="main-header">

                <a class="logo">

                    <span class="logo-lg"><b>Xsis</b>2.0</span>
                </a>

                <nav class="navbar navbar-static-top">                       

                </nav>
            </header>
        )
    }
}