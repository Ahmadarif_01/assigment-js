import React from 'react';
import { Link } from 'react-router-dom';

export default class Sidebar extends React.Component{
    render(){
        return (
            <aside class="main-sidebar">
           
            <section class="sidebar">
        
              <ul class="sidebar-menu" data-widget="tree">
                <li>
                  <a href="/Pelamar">
                    <i class="fa fa-user"></i> <span>Pelamar</span>
                  </a>
                </li>
                <li>
                  <a href="/ET">
                    <i class="fa fa-users"></i> <span>Employee Training</span>
                  </a>
                </li>
                <li>
                  <a href="/EL">
                    <i class="fa fa-users"></i> <span>Employee Leave</span>
                  </a>
                </li>
              </ul>
            </section>
          </aside>
        )
    }
}