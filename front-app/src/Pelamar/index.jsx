import React from 'react';
import Table from 'react-bootstrap/Table';
import FormInput from '..//Keahlian/formInput';
import Modal from 'react-bootstrap/Modal';
import Pelamar_p from '../Service/Pelamar_service';
import keahlian from '../Service/Keahlian_service';



class p_bio extends React.Component {

    m_perkeahlian = {
        skill_name: '',
        skill_level_id: '',
        notes: '',
        biodata_id: ''
    }

    constructor() {
        super();
        this.state = {
            open_modal: false,
            open_delete: false,
            List_biodata: [],
            List_p_bio: [],
            m_perkeahlian: [],
            open_detail: false,
            mode: '',
            m_keahlian: []
        }
    }

    loadlist = async () => {
        const respon = await Pelamar_p.getAll();
        if (respon.success) {
            this.setState({
                List_biodata: respon.result
            })
        }
    }

    buka = async (id) => {
        const respon = await keahlian.getdatabyid(id);
        const responed = await keahlian.getdatabyidbiodata(id);
        if (respon.success && responed.success) {
            this.setState({
                open_detail: true,
                m_keahlian: respon.result,
                List_p_bio: responed.result
            })
        }
    }

    cancel = () => {
        this.setState({
            open_modal: false
        })
    }


    cancel_detail = () => {
        this.setState({
            open_detail: false
        })
    }

    cancel_delete = () => {
        this.setState({
            open_delete: false
        })
    }

    edit_data = async (id) => {
        const respon = await keahlian.getdatabyidkeahlian(id);
        if (respon.success) {
            this.setState({
                mode: 'edit',
                open_modal: true,
                open_detail: false,
                m_perkeahlian: respon.result[0]
            })
        }
    }

    hendlerDel = async (id) => {
        const respon = await keahlian.getdatabyidkeahlian(id);
        if (respon.success) {
            this.setState({
                open_delete: true,
                m_perkeahlian: respon.result[0]
            })
        }
        else {
            alert(respon.result);
        }

    }


    sureDelete = async (item) => {
        const { m_perkeahlian } = this.state;
        const respon = await keahlian.delete_keahlian(m_perkeahlian);

        if (respon.success) {
            alert('Sukses' + respon.result)
        }
        else {
            alert('error' + respon.result);
        }
        this.setState({
            open_delete: false,
            open_detail:false
        })
    }

    selectHandler_keahlian = name => ({ target: { value } }) => {
        this.setState({
            m_perkeahlian: {
                ...this.state.m_perkeahlian,
                [name]: value
            }
        })
    }

    changeHandler_keahlian = name => ({ target: { value } }) => {
        this.setState({
            m_perkeahlian: {
                ...this.state.m_perkeahlian,
                [name]: value
            }
        })

    }


    simpan = async () => {
        const { m_perkeahlian, mode } = this.state;
        if (mode == 'create') {

            if (this.hendleValidation()) {
                const respon = await keahlian.post(m_perkeahlian);
            if (respon.success) {
                alert(respon.result);
            }
            else {
                alert(respon.result);
            }
            this.setState({
                open_modal: false
            })
            }

        } else {

            const respon = await keahlian.updateData(m_perkeahlian);
            if (respon.success) {
                alert(respon.result);
            }
            else {
                alert(respon.result);
            }
            this.setState({
                open_modal: false
            })
        }


    }

    componentDidMount() {
        this.loadlist();
    }

    hendelOpen = async (id) => {
        this.setState({
            mode: 'create',
            open_modal: true,
            m_perkeahlian: {
                skill_name: '',
                skill_level_id: '',
                notes: '',
                biodata_id:''
            },
            open_detail: false,
            errors: {}
        })
    }


    hendleValidation = () => {
        let fields = this.state.m_perkeahlian;
        let errors = {};
        let formIsValid = true;

        //
        if (!fields['skill_level_id']) {
            formIsValid = false;
            errors['skill_level_id'] = 'Jangan Kosong !';
        }

        this.setState({ errors: errors });
        return formIsValid;
    }


    render() {
        const { open_modal, open_detail, List_biodata, mode, m_keahlian, model_k, m_perkeahlian, errors, List_p_bio,
        open_delete } = this.state;
        return (
            <div>
                <FormInput open_modal={open_modal} mode={mode}
                    cancel={this.cancel} List_biodata={List_biodata}
                    model_k={model_k} m_keahlian={m_keahlian} m_perkeahlian={m_perkeahlian}
                    changeHandler_keahlian={this.changeHandler_keahlian}
                    selectHandler_keahlian={this.selectHandler_keahlian}
                    simpan={this.simpan} errors={errors} List_p_bio={List_p_bio} />

                <Modal show={open_delete} style={{ opacity: 1 }}>
                    <Modal.Body>
                        Apakah anda akan menghapus data ini ?
                        </Modal.Body>
                    <Modal.Footer>
                        <div class='btn-group'>
                            <button class='btn btn-success' onClick={() => this.sureDelete(m_perkeahlian.id)}>Ya</button>
                            <button class='btn btn-info' onClick={this.cancel_delete}>Tidak</button>
                        </div>
                    </Modal.Footer>
                </Modal>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            List_biodata.map(data => {
                                return (
                                    <tr>
                                        <td>{data.fullname}</td>
                                        <td>
                                            <button type="button" title='Keahlian' class="btn btn-primary" onClick={() => this.buka(data.id)}>
                                                <i class='fa fa-archive'></i>
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })

                        }
                    </tbody>

                </Table>

                <Modal show={open_detail} style={{ opacity: 1 }}>
                    <Modal.Header style={{ background: 'lightblue' }}>
                        <Modal.Header>
                        </Modal.Header>
                    </Modal.Header>
                    {/* {JSON.stringify(m_keahlian)}<br /> */}
                    {/* {JSON.stringify(List_p_bio)}<br /> */}
                    <Modal.Body>
                        <h4>Keahlian&nbsp;<button class='btn btn-primary btn-xs'>
                            <i class='fa fa-plus-circle'></i></button></h4>
                        <Table>
                            <thead>
                                <tr>
                                    <th>NAMA KEAHLIAN</th>
                                    <th>LEVEL KEAHLIAN</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    m_keahlian.map(data => {
                                        return (
                                            <tr>
                                                <td>{data.skill_name}</td>
                                                <td>{data.skill_level_id == '1' ? "Beginner" : data.skill_level_id == '2' ? "Middle" : "Expert"}</td>
                                                <td class='btn-group'>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onClick={() => this.edit_data(data.id)}>Ubah</a></li>
                                                        <li><a href="#" onClick={() => this.hendlerDel(data.id)}>Hapus</a></li>
                                                    </ul>
                                                    <button type="button" class="btn btn-info dropdown-toogle"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                        <small>More </small><i class="fa fa-sort-desc"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        )
                                    })

                                }

                            </tbody>
                        </Table>
                    </Modal.Body>
                    <Modal.Footer>
                        <button type='button' class='btn btn-danger' onClick={this.cancel_detail}>Close</button>
                    </Modal.Footer>
                </Modal>
            </div>

        )
    }
}
export default p_bio;