import React from 'react';
import Modal from 'react-bootstrap/Modal';
import 'bootstrap/dist/css/bootstrap.min.css';


class FormInput extends React.Component {
    render() {
        const { open_modal, mode, cancel, m_perkeahlian, selectHandler_keahlian
            , changeHandler_keahlian, simpan, errors, List_p_bio } = this.props;
        return (
            <Modal show={open_modal} style={{ opacity: 1 }}>
                <Modal.Header style={{ backgroundColor: 'lightblue' }}>
                    <Modal.Title>{mode === 'create' ? <label>Tambah Data Keahlian</label> : <label>Edit Data Keahlian</label>}</Modal.Title>
                </Modal.Header>
                {/* {JSON.stringify(m_perkeahlian)}<br /> */}
                {/* {JSON.stringify(List_p_bio)}<br /> */}
                <Modal.Body>
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <small>Nama Keahlian</small>
                                <input type="text" class="form-control" value={m_perkeahlian.skill_name} onChange={changeHandler_keahlian('skill_name')} />
                            </div>
                            <div class="form-group col-md-6">
                                <small>Level Keahlian *</small>
                                <select class='form-control' onChange={selectHandler_keahlian('skill_level_id')}>
                                    <option disabled selected>- Pilih -</option>
                                    <option value='1' selected={m_perkeahlian.skill_level_id === '1'}>Beginner</option>
                                    <option value='2' selected={m_perkeahlian.skill_level_id === '2'}>Middle</option>
                                    <option value='3' selected={m_perkeahlian.skill_level_id === '3'}>Expert</option>
                                </select>
                                {/* <span style={{ color: "red" }}>{errors["skill_level_id"]}</span> */}
                            </div>
                        </div>
                        <div class="form-group">
                            <small>Catatan</small>
                            <textarea type="text" class="form-control" onChange={changeHandler_keahlian('notes')}>{m_perkeahlian.notes}</textarea>
                           
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <div class='btn-group'>
                        <button class='btn btn-danger' onClick={cancel}>Batal</button>
                        <button class='btn btn-primary' onClick={simpan}>Simpan</button>
                    </div>
                </Modal.Footer>
            </Modal>

        )
    }
}

export default FormInput;